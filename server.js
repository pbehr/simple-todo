const express = require('express'),
      app = express();

app.set('views', 'views');
app.set('view engine', 'ejs');

app.use(express.static('public'));
app.use(express.urlencoded({extended:false}));

const mysql = require('mysql')
const dbConn = mysql.createConnection({
      host: 'localhost',
      user: 'testuser',
      password: 'testpassword',
      database: 'testdb'
});
dbConn.connect();


// Homepage
app.get('/', function(req, res) {
    res.render('index');
});

// List tasks
app.get('/list', (req, res) => {
      dbConn.query("SELECT id, task, due \
                    FROM todo \
                    WHERE completed IS NULL", function (err, rows) {
            if (err) {
                  res.render('error', { error : err });
            } else {
                  res.render('list', {list : rows});
            }
      });
});

// Add a new task
app.post('/task', (req, res) => {
      let sql = "INSERT INTO todo (task, due) VALUES('"+ req.body.task + "','" + req.body.due + "')";
      dbConn.query(sql, function (err, rows) {
            if (err) {
                  res.render('error', { error : err });
            } else {
                  res.redirect(301, '/list');
            }
      });
});

// Complete a task
app.post('/task/:id', (req, res) => {
      let sql = "UPDATE todo SET completed = current_date WHERE id = " + req.params.id;
      dbConn.query(sql, function (err, rows) {
            if (err) {
                  res.render('error', { error : err });
            } else {
                  res.redirect(301, '/list');
            }
      });
});


app.listen(3000, function(){
	console.log('Listening on port 3000');
});
